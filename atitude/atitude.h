#ifndef ATITUDE_HEADER_
#define ATITUDE_HEADER_

#define PI 3.14159265

int atitude_accel(int file, double *offset, double * angulos);
int atitude_gyro(int file, double *offset, double *angulos,  double dt);

#endif