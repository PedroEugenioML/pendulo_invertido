#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include "atitude.h"
#include "../sensor/gyroscope.h"
#include "../sensor/accelerometer.h"


int atitude_accel(int file, double *offset, double * angulos){

	double ax, ay, az;
    double bruto[3];
	if(leitura_accel(file, offset, bruto) < 0)
	{
		printf("Falha na leitura do acelerômetro\n");
		return -1;
	}
	else
	{
		//printf("%d X:%f Y:%f Z:%f\n", i, bruto[0], bruto[1], bruto[2]);
		ax = bruto[0];
		ay = bruto[1];
		az = bruto[2];
		
		angulos[0] = (atan2(ax, sqrt(pow(ay,2) + pow(az,2)))*180) / PI;
		angulos[1] = (atan2(ay, sqrt(pow(ax,2) + pow(az,2)))*180) / PI;
		angulos[2] = (atan2(az, sqrt(pow(ax,2) + pow(ay,2)))*180) / PI;
		
		return 0;
	}
};

int atitude_gyro(int file, double *offset, double *angulos,  double dt){
    
	double gx, gy, gz;
	double bruto[3];
	if(leitura_gyro(file, offset, bruto) < 0)
	{
		printf("Falha na leitura do giroscópio\n");
		return -1;
	}
	else
    {
		//printf("%d X:%f Y:%f Z:%f\n", i, bruto[0], bruto[1], bruto[2]);
		gx = bruto[0];
    	gy = bruto[1];
		gz = bruto[2];
		
		angulos[0] = angulos[0] + gx*dt;
		angulos[1] = angulos[1] + gy*dt;
		angulos[2] = angulos[2] + gz*dt;
		
		return 0;
	}
};		