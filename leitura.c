#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <pthread.h>
#include <stdint.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <stdbool.h>
#include "auxiliar/auxiliar.h"
#include "sensor/MPU9250.h"
#include "sensor/gyroscope.h"
#include "sensor/accelerometer.h"
#include "sensor/magnetometer.h"

#define PI 3.14159265
#define HPF 0.95
#define LPF 0.05

int main(int argc, char * argv[]){

	signal(SIGINT, saida);

	// Variaveis de acesso
	int file;
	
	FILE * dados_gyro;
	FILE * dados_accel;
	FILE * dados_filtro;
	
	double offset_accel[3];
	double offset_gyro[3];
	//double scale[3];
	double bruto[3];
	
	printf("Iniciando setup\n");
	// Verificando comunicacao com sensores
	//printf("\nIniciando comunicacao I2C\n");
	if((file=open("/dev/i2c-2", O_RDWR)) < 0){
		perror("Falha no acesso a porta I2C-1\n");
		return 1;
	}
	printf("Arquivo i2c-2 aberto\n");
	
	//dados=open("/var/lib/cloud9/PedroEugenioTCC/giroscopio_bruto.txt", O_RDWR | O_CREAT);
	
	dados_gyro=fopen("gyro.txt", "w");
	if(dados_gyro == NULL)
	{
		printf("Falha na abertura do arquivo x\n");
		return 1;
	}
	dados_accel=fopen("accel.txt", "w");
	if(dados_accel == NULL)
	{
		printf("Falha na abertura do arquivo y\n");
		return 1;
	}
	dados_filtro=fopen("filtro.txt", "w");
	if(dados_filtro == NULL)
	{
		printf("Falha na abertura do arquivo z\n");
		return 1;
	}
	printf("Arquivo aberto\n");
	
	if(inicia_MPU9250(file)<0)
	{
		printf("Falha na inicialização da MPU9250\n");
		return 1;
	}
	printf("IMU iniciada\n");
	if(config_MPU9250(file, GYRO_FULL_SCALE_250_DPS, ACCEL_FULL_SCALE_2_G) < 0)
	{
		printf("Falha na configuração da MPU9250\n");
		return 1;
	}
	
	
	if(calibra_gyro(file, offset_gyro)<0)
	{
		printf("Erro na calibração\n");
		return 1;
	}
	
	
    if(calibra_accel(file, offset_accel)<0)
	{
		printf("Erro na calibração\n");
		return 1;
	}
	/*
	printf("Offset giro calculado\tX:%f Y:%f Z:%f\n", offset[0], offset[1], offset[2]);
    
	
    if(calibra_mag(file, offset, scale)<0)
	{
		printf("Erro na calibração\n");
		return 1;
	}
	*/
	//printf("Offset mag calculado\tX:%f Y:%f Z:%f\n", offset_accel[0], offset_accel[1], offset_accel[2]);
    
	printf("IMU configurada\n");
	sleep(3);
    printf("iniciando leitura dos dados\n\n");
    int i;
    double ax, ay, az, AccXangle, AccYangle, AccZangle;
    double gx, gy, gz, GyroXangle = 0, GyroYangle = 0, GyroZangle = 0; 
    double angleX, angleY, angleZ;
    double past_time = (clock()/CLOCKS_PER_SEC), current_time = 0, dt = 0;
    
    printf("Inicando leitura dos dados:\n");
    usleep(3000000);
    printf("3...\n");
    usleep(1000000);
    printf("2...\n");
    usleep(1000000);
    printf("1...\n");
    
	for(i=0; i<1000; i++)
	{
		current_time = (clock ()/CLOCKS_PER_SEC);
		dt = current_time - past_time;
		past_time = current_time;
		
		if(leitura_accel(file, offset_accel, bruto) < 0)
		{
			printf("Falha na leitura do acelerômetro\n");
			i--;
			continue;
		}
		else
		{
			//printf("%d X:%f Y:%f Z:%f\n", i, bruto[0], bruto[1], bruto[2]);
			ax = bruto[0];
			ay = bruto[1];
			az = bruto[2];
			
			AccXangle = (atan2(ax, sqrt(pow(ay,2) + pow(az,2)))*180) / PI;
			AccYangle = (atan2(ay, sqrt(pow(ax,2) + pow(az,2)))*180) / PI;
			AccZangle = (atan2(az, sqrt(pow(ax,2) + pow(ay,2)))*180) / PI;
			
		}
		
		
		if(leitura_gyro(file, offset_gyro, bruto) < 0)
		{
			printf("Falha na leitura do giroscópio\n");
			i--;
			continue;
		}
		else
		{
			//printf("%d X:%f Y:%f Z:%f\n", i, bruto[0], bruto[1], bruto[2]);
			gx = bruto[0];
			gy = bruto[1];
			gz = bruto[2];
			
			GyroXangle = angleX + gx*0.01;
			GyroYangle = angleY + gy*0.01;
			GyroZangle = angleZ + gz*0.01;
			
		}
		
		angleX = HPF*(angleX + gx*(0.01)) + LPF*AccXangle;
		angleY = HPF*(angleY + gy*(0.01)) + LPF*AccYangle;
		angleZ = HPF*(angleZ + gz*(0.01)) + LPF*AccZangle;
		
		printf("angleX: %d angleY: %d angleZ: %d\n", (int ) angleX, (int ) angleY, (int ) angleZ);
		
		fprintf(dados_gyro,"%d ", (int ) GyroXangle);
		fprintf(dados_accel,"%d ", (int ) AccXangle);
		fprintf(dados_filtro,"%d ", (int ) angleX);
		
		usleep(10000);
	}
	
	close(file);
	
	fclose(dados_gyro);
	fclose(dados_accel);
	fclose(dados_filtro);
	
	return 0;
}