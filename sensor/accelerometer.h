#ifndef ACCELEROMETER_HEADER_
#define ACCELEROMETER_HEADER_

// Escalas para o acelerometro
#define         ACCEL_FULL_SCALE_2_G            0X00
#define         ACCEL_FULL_SCALE_4_G            0X08
#define         ACCEL_FULL_SCALE_8_G            0X10
#define         ACCEL_FULL_SCALE_16_G           0X18

// Taxas de conversao das unidades dos sensores
#define		SENSITIVITY_ACCEL		2.0/32768.0

// Maximos e minimos para calcular offset
double max_accelx, max_accely, max_accelz;
double min_accelx, min_accely, min_accelz;


// Variaveis de offset
double offset_accelx, offset_accely, offset_accelz;

// Valores brutos do tipo int
int16_t raw_accelx, raw_accely, raw_accelz;

// Valores brutos do tipo double
double Raw_accelx, Raw_accely, Raw_accelz;

// Variaveis para valores escalizados
double accelx, accely, accelz;

int calibra_accel(int file, double * offset_gyro);
int leitura_accel(int file, double * offset_accel, double * accel);

#endif
