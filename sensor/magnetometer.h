#ifndef MAGNETOMETER_HEADER_
#define MAGNETOMETER_HEADER_

#define		SENSITIVITY_MAG			(10.0*4800.0)/32768.0

// Maximos e minimos para calcular offset
double max_magx, max_magy, max_magz;
double min_magx, min_magy, min_magz;

// Variaveis de offset
double offset_magx, offset_magy, offset_magz;
double scale_magx, scale_magy, scale_magz, avg_scale;

// Valores brutos do tipo int
int16_t raw_magx, raw_magy, raw_magz;

// Valores brutos do tipo double
double Raw_magx, Raw_magy, Raw_magz;

// Variaveis para valores escalizados
double magx, magy, magz;

int calibra_mag(int file, double * offset_mag, double * scale_mag);
int leitura_mag(int file, double * offset_mag, double * scale_mag, double * mag);

#endif