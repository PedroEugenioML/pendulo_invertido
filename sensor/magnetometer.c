#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <stdint.h>
#include <unistd.h>
#include <time.h>
#include <stdbool.h>
#include "../auxiliar/auxiliar.h"
#include "MPU9250.h"
#include "magnetometer.h"


int calibra_mag(int file, double * offset_mag, double * scale_mag)
{
    // Variaveis de controle
	int tempo, t_inicial, t_final;
	t_inicial = sec();
	t_final = 20; // Loop de 10 segundos
	tempo = 0;

    char buf_mag[7];
    char teste1, teste2;
    
    max_magx = -32768.0; max_magy = -32768.0; max_magz = -32768.0;
    min_magx = 32768.0; min_magy = 32768.0; min_magz = 32768.0;
    
    offset_magx = 0.0; offset_magy = 0.0; offset_magz = 0.0;
    scale_magx = 0.0; scale_magy = 0.0; scale_magz = 0.0; avg_scale = 0.0;
	
	printf("calibrando magnetometro\n");
    printf("Calulando offset para os eixos X e Y\n");
	printf("Posicione o MPU horizontalmente e o rotacione\n");
	sleep(5);
	printf("GIRANDO\n");
	
	while(tempo<t_final)
	{
		
		leitura_reg(file, MAG_ADDRESS, 0x02, 1, &teste1);
		teste1 = teste1 & 0x01;	// mascara para DRDY do ST1, verifica se os dados estão prontos para leitura
		if(teste1 == 0x01)
		{
			if(leitura_reg(file, MAG_ADDRESS, 0x03, 7, buf_mag)<0)
			{
				printf("Falha na amostragem do magnetometro\n");
			}
			teste2 = buf_mag[6] & 0x08; // mascara para bit de overflow
			if(teste2 != 0x08)
			{
			    raw_magx = ((int16_t)buf_mag[3] << 8 | (int16_t)buf_mag[2]); 
        		raw_magy = ((int16_t)buf_mag[1] << 8 | (int16_t)buf_mag[0]); 
        		//raw_magz = -((int16_t)buf_mag[5] << 8 | (int16_t)buf_mag[4]);
			}

            // Casting de variaveis
    	    Raw_magx = (double)raw_magx;
    	    Raw_magy = (double)raw_magy;
    
    	    //Maximos e minimos para o Magnetometro em cada um dos seus eixos (X,Y e Z)
    	    //Eixo X
    	    if (Raw_magx >= max_magx){
    		    max_magx = Raw_magx;
      		}	
      		if (Raw_magx <= min_magx){
        		min_magx = Raw_magx;
      		}
          	//Eixo Y
          	if (Raw_magy >= max_magy){
            	max_magy = Raw_magy;
          	}
          	if (Raw_magy <= min_magy){
            	min_magy = Raw_magy;
          	}

		}		
		tempo = sec() - t_inicial;
	}

	printf("Calculando offset para o eixo Z\n");
	printf("Posicioone o MPU verticalmente e o rotacione\n");
	sleep(5);
	printf("GIRANDO\n");
	tempo = 0;
	t_inicial = sec();

	while(tempo<t_final)
    {
    	//printf("Z");
        // Calibrando magnetometro para alta resolsucao
        leitura_reg(file, MAG_ADDRESS, 0x02, 1, &teste1);
        teste1 = teste1 & 0x01;           // mascara para DRDY do ST1
        if(teste1 == 0x01)
        {
            if(leitura_reg(file, MAG_ADDRESS, 0x03, 7, buf_mag)<0)
            {
                printf("Falha na amostragem do magnetometro\n");
            }
            teste2 = buf_mag[6] & 0x08; // mascara para bit de overflow
            if(teste2 != 0x08)
            {
                //raw_magx = ((int16_t)buf_mag[3] << 8 | (int16_t)buf_mag[2]); 
        		//raw_magy = ((int16_t)buf_mag[1] << 8 | (int16_t)buf_mag[0]); 
        		raw_magz = -((int16_t)buf_mag[5] << 8 | (int16_t)buf_mag[4]);

                // Casting de variaveis
                Raw_magz = (double)raw_magz;
                                
                //Maximos e minimos para o Magnetometro no eixo Z
                //Eixo X
                if (Raw_magz >= max_magz){
                        max_magz = Raw_magz;
                }
                if (Raw_magz <= min_magz){
                        min_magz = Raw_magz;
                }
            }
        }
        tempo = sec() - t_inicial;
    }

	  // Offset e escala do magnetometro no eixo Z
	offset_mag[0] = (max_magx + min_magx) / 2;
	offset_mag[1] = (max_magy + min_magy) / 2;
    offset_mag[2] = (max_magz + min_magz) / 2;

	printf("X max:%.2f min:%.2f\n", max_magx, min_magx);
    printf("y max:%.2f min:%.2f\n", max_magy, min_magy);
    printf("z max:%.2f min:%.2f\n", max_magz, min_magz);


	scale_magx = (max_magx - min_magx) / 2;
	scale_magy = (max_magy - min_magy) / 2;
    scale_magz = (max_magz - min_magz) / 2;

	avg_scale = (scale_magx + scale_magy + scale_magz) / 3;

    printf("Scale X:%.2f Y:%.2f Z:%.2f avg:%.2f\n", scale_magx, scale_magy, scale_magz, avg_scale);

	scale_mag[0] = avg_scale / scale_magx;
	scale_mag[1] = avg_scale / scale_magy;
	scale_mag[2] = avg_scale / scale_magz;

	printf("Offset X:%.2f Y:%.2f Z:%.2f\n", offset_magx, offset_magy, offset_magz);
	printf("Scale X:%.2f Y:%.2f Z:%.2f\n", scale_magx, scale_magy, scale_magz);

    printf("Sucesso na calibração do magnetômetro\n");
    return 1;
}

int leitura_mag(int file, double * offset_mag, double * scale_mag, double * mag)
{
    char buf_mag[7];
    char teste1, teste2;
    
    escrita_reg(file, MAG_ADDRESS, 0x0A, 0x11);
    leitura_reg(file, MAG_ADDRESS, 0x02, 1, &teste1);
    teste1 = teste1 & 0x01;           // mascara para DRDY do ST1
    if(teste1 == 0x01)
    {
        if(leitura_reg(file, MAG_ADDRESS, 0x03, 7, buf_mag)<0)
        {
            printf("Falha na leitura do magnetometro\n");
            return -1;
        }
        teste2 = buf_mag[6] & 0x08; // mascara para bit de overflow
        if(teste2 != 0x08)
        {
            raw_magx = ((int16_t)buf_mag[3] << 8 | (int16_t)buf_mag[2]); 
        	raw_magy = ((int16_t)buf_mag[1] << 8 | (int16_t)buf_mag[0]); 
        	raw_magz = -((int16_t)buf_mag[5] << 8 | (int16_t)buf_mag[4]);
        }
    }
	Raw_magx = (double)raw_magx; 
	Raw_magy = (double)raw_magy; 
	Raw_magz = (double)raw_magz;

	mag[0] = ((Raw_magx - offset_mag[0]) * scale_mag[0]) * SENSITIVITY_MAG;
	mag[1] = ((Raw_magy - offset_mag[1]) * scale_mag[1]) * SENSITIVITY_MAG;
	mag[2] = ((Raw_magz - offset_mag[2]) * scale_mag[2]) * SENSITIVITY_MAG; //mG
	
    return 1;
}