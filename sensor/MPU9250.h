#ifndef MPU9250_HEADER_
#define MPU9250_HEADER_

// Endereço dos sensores no barramento I2C
#define MPU9250_ADDRESS 0x68
#define MAG_ADDRESS	0x0c

// Funções gerais usadas com a MPU9250
int inicia_i2c();
int inicia_MPU9250(int flie);
int config_MPU9250(int file, char scale_gyro, char scale_accel);
int leitura_reg(int file, char address, char reg, int size, char * read_buffer);
int escrita_reg(int file, char address, char reg, char buf);

#endif
