#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <stdint.h>
#include <unistd.h>
#include <stdbool.h>
#include "../auxiliar/auxiliar.h"
#include "MPU9250.h"
#include "gyroscope.h"
#include "accelerometer.h"
#include "magnetometer.h"

int inicia_sensor(int *file, double * offset_accel, double * offset_gyro){
	if( (*file=inicia_i2c()) <= 0){
		printf("Falha na abertura do I2C\n");
		return -1;
	}
	printf("I2C iniciada\n");
	
	if(inicia_MPU9250(*file)<0)
	{
		printf("Falha na inicialização da MPU9250\n");
		return -1;
	}
	printf("IMU iniciada\n");
	
	if(config_MPU9250(*file, GYRO_FULL_SCALE_250_DPS, ACCEL_FULL_SCALE_2_G) < 0)
	{
		printf("Falha na configuração da MPU9250\n");
		return -1;
	}
	
	if(calibra_gyro(*file, offset_gyro)<0)
	{
		printf("Erro na calibração\n");
		return -1;
	}
	
    if(calibra_accel(*file, offset_accel)<0)
	{
		printf("Erro na calibração\n");
		return -1;
	}
	/*
    if(calibra_mag(*file, offset_mag, scale_mag)<0)
	{
		printf("Erro na calibração\n");
		return 1;
	}
	*/
	
	return 0;
};
