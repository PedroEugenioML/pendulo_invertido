#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <unistd.h>
#include "MPU9250.h"


int inicia_i2c(){
	int file;
	if((file=open("/dev/i2c-2", O_RDWR)) < 0){
		perror("Falha no acesso a porta I2C-2\n");
		return -1;
	}
	printf("Arquivo i2c-2 aberto\n");
	return file;
};

int inicia_MPU9250(int file)
{
    char whoami;
	//printf("\nAtivando giroscopio\n");
	if(escrita_reg(file, MPU9250_ADDRESS, 0x6b,  0x00)<0)
	{
		printf("Falha na ativacao da IMU\n");
		return -1;
	}

	//printf("Acessando giroscopio\n");
	if(leitura_reg(file, MPU9250_ADDRESS, 0x75, 1, &whoami)<0)
	{
		printf("Falha na leitura do sensor\n");
		return -1;
	}
	printf("Quem eh: 0x%02x\n", whoami);

	//printf("\nTentando ativar magnetometro\n");
	if(escrita_reg(file, MPU9250_ADDRESS, 0x37, 0x02)<0)
	{
		printf("Falha na ativacao do magnetometro\n");
		return -1;
	}
	if(escrita_reg(file, MAG_ADDRESS, 0x0A, 0x16)<0)
	{
		printf("Falha na configuração da escala\n");
	}

	//printf("Acessando magnetometro\n");
	if(leitura_reg(file, MAG_ADDRESS, 0x00, 1, &whoami)<0)
	{
		printf("Falha no acesso ao magnetometro\n");
		return -1;
	}
	printf("Quem eh: 0x%02x\n\n", whoami);
	//printf("Tudo certo com o sensor\n\n");
	
	return 1;
}


int config_MPU9250(int file, char scale_gyro, char scale_accel)
{
	
	//  Configurando sensores
	
    if(escrita_reg(file, MAG_ADDRESS, 0x0A, 0x16)<0)
	{
		printf("Falha na configuração da escala do magnetometro");
		return -1;
	}
	if(escrita_reg(file, MPU9250_ADDRESS, 0x1B, scale_gyro)<0)
	{
		printf("Falha na configuracao do giroscopio\n");
		return -1;
	}
	if(escrita_reg(file, MPU9250_ADDRESS, 0x1C, scale_accel)<0)
	{
		printf("Falha na configuracao do acelerometro\n");
		return -1;
	}
	printf("Sucesso na configuracao dos sensores\n\n");
	return 1;

}

// Funcoes de leitura e escrita nos registradores dos dispositivos i2c
int leitura_reg(int file, char address, char reg, int size, char * read_buffer)
{
	char write_buffer[1] = {reg};
	//printf("\nIniciando leitura\n");
	if(ioctl(file, I2C_SLAVE, address)<0)
	{
		printf("falha no sensor 0x%02x\n", address);
		return -1;
	}
	else
	{
		//printf("Sucesso no acesso do sensor 0x%02x\n", address);
		if(write(file, write_buffer, 1)!=1)
		{
			printf("Falha no acesso ao registrador 0x%02x\n", reg);
			return -1;
		}
		else
		{
			//printf("Sucesso no acesso do registrador 0x%02x\n", reg);
			if(read(file, read_buffer, size)!=size)
			{
				printf("Falha na leitura do resgistrador 0x%02x\n", reg);
				return -1;
			}
		}
	}
	return 1;
}


int escrita_reg(int file, char address, char reg, char buf)
{
	char write_buffer[2];
	write_buffer[0] = reg;
	write_buffer[1] = buf;
	//printf("\nIniciando escrita\n");
	if(ioctl(file, I2C_SLAVE, address)<0)
    {
    	printf("falha no acesso do sensor 0x%02x\n", address);
        return -1;
    }
    else
    {
		//printf("Sucesso no acesso do sensor 0x%02x\n", address);
        if(write(file, write_buffer, 2) != 2)
        {
            printf("Falha no acesso ao registrador 0x%02x\n", reg);
        	return -1;
        }
        else
        {
        //printf("sucesso na escrita no resgistrador 0x%02x\n", reg);
        return 1;
        }
	}
}
