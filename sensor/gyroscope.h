#ifndef GYROSCOPE_HEADER_
#define GYROSCOPE_HEADER_

// Escala do para o giroscopio
#define		GYRO_FULL_SCALE_250_DPS		0X00
#define		GYRO_FULL_SCALE_500_DPS		0X08
#define 	GYRO_FULL_SCALE_1000_DPS	0X10
#define		GYRO_FULL_SCalE_2000_DPS	0X18

// Taxas de conversao das unidades dos sensores
#define 	SENSITIVITY_GYRO		250.0/32768.0

// Maximos e minimos para calcular offset
double max_gyrox, max_gyroy, max_gyroz;
double min_gyrox, min_gyroy, min_gyroz;

// Variaveis de offset
double offset_gyrox, offset_gyroy,  offset_gyroz;

// Valores brutos do tipo int
int16_t raw_gyrox, raw_gyroy, raw_gyroz;

// Valores brutos do tipo double
double Raw_gyrox, Raw_gyroy, Raw_gyroz;

// Variaveis para valores escalizados
double gyrox, gyroy, gyroz;

// Funções de uso do giroscópio
int calibra_gyro(int file, double * offset_gyro);
int leitura_gyro(int file, double * offset_gyro, double * gyro);

#endif
