#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <stdint.h>
#include <unistd.h>
#include "MPU9250.h"
#include "accelerometer.h"


int calibra_accel(int file, double * offset_accel)
{
    // Variaveis de controle
    char buffer[6];
    int i = 0;
    int amostras_offset = 1000;
    
    offset_accelx = 0;
    offset_accely = 0;
	offset_accelz = 0;
	    
    for(i=0;i<amostras_offset;i++)
    {
        // Lendo amostras descartáveis.
        if(leitura_reg(file, MPU9250_ADDRESS, 0x3b, 6, buffer) < 0)
        {
            printf("Falha na obtencao de dados\n");
            return -1;
        }
    }
    
    for(i=0;i<amostras_offset;i++)
    {
    
        // Lendo sensores
        if(leitura_reg(file, MPU9250_ADDRESS, 0x3b, 6, buffer) < 0)
        {
            printf("Falha na obtencao de dados\n");
            return -1;
        }
        // printf("Amostras lidas\n");
    
        // Montando dados dos sensores
        raw_accelx = (((int16_t)buffer[0])<<8) | ((int16_t)buffer[1]);
        raw_accely = (((int16_t)buffer[2])<<8) | ((int16_t)buffer[3]);
        raw_accelz = (((int16_t)buffer[4])<<8) | ((int16_t)buffer[5]);
    
        // Casting das variaveis
        Raw_accelx = (double)raw_accelx;
        Raw_accely = (double)raw_accely; 
        Raw_accelz = (double)raw_accelz;

        offset_accelx += Raw_accelx;
	    offset_accely += Raw_accely;
	    offset_accelz += Raw_accelz;
    	usleep(1);
    }
    
    // Calculando offset
    //Giroscopio
    
    offset_accel[0] = (offset_accelx/1000);
    offset_accel[1] = (offset_accely/1000);
    offset_accel[2] = ((offset_accelz/1000) - 16384);
    
    //printf("Offset do acelerometro\tX:%.2f Y:%.2f Z:%.2f\n",offset_accelx, offset_accely, offset_accelz);
    //printf("Offset do giroscopio\tX:%.2f Y:%.2f Z:%.2f\n",offset_gyrox, offset_gyroy, offset_gyroz);
    
    printf("Sucesso na calibração do acelerômetro\n");
    return 1;
}

int leitura_accel(int file, double * offset_accel, double * accel)
{
    char buffer[6];
    
    // Lendo sensores
    if(leitura_reg(file, MPU9250_ADDRESS, 0x3b, 6, buffer) < 0)
    {
        printf("Falha na obtencao de dados\n");
        return -1;
    }
    // printf("Amostras lidas\n");

    // Montando dados dos sensores
    raw_accelx = (((int16_t)buffer[0])<<8) | ((int16_t)buffer[1]);
    raw_accely = (((int16_t)buffer[2])<<8) | ((int16_t)buffer[3]);
    raw_accelz = (((int16_t)buffer[4])<<8) | ((int16_t)buffer[5]);

    // Casting das variaveis
    Raw_accelx = (double)raw_accelx; 
    Raw_accely = (double)raw_accely; 
    Raw_accelz = (double)raw_accelz;

    accel[0] = ((Raw_accelx - offset_accel[0]) * SENSITIVITY_ACCEL);
	accel[1] = ((Raw_accely - offset_accel[1]) * SENSITIVITY_ACCEL);
	accel[2] = ((Raw_accelz - offset_accel[2]) * SENSITIVITY_ACCEL);// Unidade [g]
	
    return 1;
}
