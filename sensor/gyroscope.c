#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <stdint.h>
#include <unistd.h>
#include "MPU9250.h"
#include "gyroscope.h"


int calibra_gyro(int file, double * offset_gyro)
{
    // Variaveis de controle
    char buffer[6];
    int i = 0;
    int amostras_offset = 1000;
    
    offset_gyrox = 0;
    offset_gyroy = 0;
	offset_gyroz = 0;
	    
    for(i=0;i<amostras_offset;i++)
    {
        // Lendo amostras descartáveis.
        if(leitura_reg(file, MPU9250_ADDRESS, 0x43, 6, buffer) < 0)
        {
            printf("Falha na obtencao de dados\n");
            return -1;
        }
    }
    
    for(i=0;i<amostras_offset;i++)
    {
    
        // Lendo sensores
        if(leitura_reg(file, MPU9250_ADDRESS, 0x43, 6, buffer) < 0)
        {
            printf("Falha na obtencao de dados\n");
            return -1;
        }
        // printf("Amostras lidas\n");
    
        // Montando dados dos sensores
        raw_gyrox = (((int16_t)buffer[0])<<8) + ((int16_t)buffer[1]);
        raw_gyroy = (((int16_t)buffer[2])<<8) + ((int16_t)buffer[3]);
        raw_gyroz = (((int16_t)buffer[4])<<8) + ((int16_t)buffer[5]);
    
        // Casting das variaveis
        Raw_gyrox = (double)raw_gyrox; Raw_gyroy = (double)raw_gyroy; Raw_gyroz = (double)raw_gyroz;

        offset_gyrox += Raw_gyrox;
	    offset_gyroy += Raw_gyroy;
	    offset_gyroz += Raw_gyroz;
    	usleep(1);
    }
    
    // Calculando offset
    //Giroscopio
    
    offset_gyro[0] = offset_gyrox/1000;
    offset_gyro[1] = offset_gyroy/1000;
    offset_gyro[2] = offset_gyroz/1000;
    
    //printf("Offset do acelerometro\tX:%.2f Y:%.2f Z:%.2f\n",offset_accelx, offset_accely, offset_accelz);
    //printf("Offset do giroscopio\tX:%.2f Y:%.2f Z:%.2f\n",offset_gyrox, offset_gyroy, offset_gyroz);
    
    printf("Sucesso na calibração do giroscópio\n");
    return 1;
}

int leitura_gyro(int file, double * offset_gyro, double * gyro)
{
    char buffer[6];
    
    // Lendo sensores
    if(leitura_reg(file, MPU9250_ADDRESS, 0x43, 6, buffer) < 0)
    {
        printf("Falha na obtencao de dados\n");
        return -1;
    }
    // printf("Amostras lidas\n");

    // Montando dados dos sensores
    raw_gyrox = (((int16_t)buffer[0])<<8) + ((int16_t)buffer[1]);
    raw_gyroy = (((int16_t)buffer[2])<<8) + ((int16_t)buffer[3]);
    raw_gyroz = (((int16_t)buffer[4])<<8) + ((int16_t)buffer[5]);

    // Casting das variaveis
    Raw_gyrox = (double)raw_gyrox; 
    Raw_gyroy = (double)raw_gyroy; 
    Raw_gyroz = (double)raw_gyroz;

    gyro[0] = ((Raw_gyrox - offset_gyro[0]) * SENSITIVITY_GYRO);
	gyro[1] = ((Raw_gyroy - offset_gyro[1]) * SENSITIVITY_GYRO);
	gyro[2] = ((Raw_gyroz - offset_gyro[2]) * SENSITIVITY_GYRO);// Unidade [graus/s]
	
    return 1;
}
