#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <pthread.h>
#include <stdint.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include <stdbool.h>
#include "auxiliar/auxiliar.h"
#include "sensor/sensor.h"
#include "sensor/MPU9250.h"
#include "sensor/gyroscope.h"
#include "sensor/accelerometer.h"
#include "sensor/magnetometer.h"
#include "atitude/atitude.h"
#include "filtro/filtro.h"
#include "motor/motor.h"
#include "PRU/timer/timer.h"
#include "PRU/encoder/encoder.h"
#include "PRU/PRU.h"

double dt = 0.01;
double setpoint[3] = {0,0,0};

// IMU

int file; 
double offset_accel[3];
double offset_gyro[3];

// Motores

int pos_motorA = 0, pos_motorB = 0;
bool direcao_A = false, direcao_B = false;
static unsigned int *encoder;

// PID

float kp = 2.9;
float ki = 0.0;
float kd = 0.0;
    
float erro_k = 0.0, erro_k_1 = 0.0, erro_k_2 = 0.0;
    
float termo_proporcional = 0.0;
float termo_integral = 0.0;
float termo_derivativo = 0.0;
    
float anterior = 0.0;

// Análise de tempo
struct timespec tStart, tEnd;
    
void alarme_funcao(int signum){

	clock_gettime(CLOCK_REALTIME, &tEnd);
	
    double angulos_filtrados[3];
    int sinal_controle;
    
    if(filtro_complementar(file, offset_accel, offset_gyro, angulos_filtrados, dt)<0)
	{
		printf("Falha na filtragem dos dados\n");
	}
	leitura_encoder(&encoder, &pos_motorB, &pos_motorA);
		
	erro_k = setpoint[0] - angulos_filtrados[0];
		
	// Cálculo da parcela proporcional
	termo_proporcional = kp * erro_k;
		
	// Cálculo da parcela integral
	termo_integral +=  ki * erro_k;
		
	// Cálculo da parcela derivativa
	//termo_derivativo = ( kd/dt ) * (erro_k - (2* erro_k_1) + erro_k_2);
	printf("erro: %f integral: %f d_erro: %f\n", erro_k, termo_integral, termo_derivativo);
		
	sinal_controle = (int ) ( termo_proporcional + termo_integral + termo_derivativo );
		
	if(sinal_controle>100){
		sinal_controle = 100;
	}
	if(sinal_controle<-100){
		sinal_controle = -100;
	}
		
	if(sinal_controle>0){
		direcao_A = true;
		direcao_B = false;
	}
	else
	{
		direcao_A = false;
		direcao_B = true;
	}
	direcao_motorA(direcao_A);
    direcao_motorB(direcao_B);
    PWM_MotorA(abs(sinal_controle));
    PWM_MotorB(abs(sinal_controle));
	
	printf("X: %f Y: %f Z: %f sinal: %d, enc_A: %d, enc_B: %d\n", angulos_filtrados[0], angulos_filtrados[1], angulos_filtrados[3], sinal_controle, pos_motorA, pos_motorB);

		
	// Atualiza variáveis
	anterior = angulos_filtrados[0];
		
	
	printf("Tempo: %f\n", ((tEnd.tv_sec - tStart.tv_sec) + (tEnd.tv_nsec - tStart.tv_nsec)/1E9) *1000);
    
    clock_gettime(CLOCK_REALTIME, &tStart);
    ualarm(1000, 0);  
};

void funcao_final(int signum)
{
    ualarm(0, 0);
    
	desliga_motores();
	encerra_timer();
	close(file);
	
	signal(SIGINT, SIG_DFL);
};

int main(int argc, char * argv[]){

	signal(SIGINT, funcao_final);
	signal(SIGALRM, alarme_funcao);
	
	inicia_sensor(&file, offset_accel, offset_gyro);
	printf("IMU configurada\n");
	
    inicia_PRU();
    //inicia_timer(periodo);
    inicia_encoder(&encoder);
    printf("PRU configurado\n");
    
    config_pinos_PWM();
    inicia_PWM();
    printf("PWM configurado\n");
    
    liga_motores();
	printf("Motores ligados\n");
	sleep(3);
	
	clock_gettime(CLOCK_REALTIME, &tStart);
	ualarm(1000, 0);
    
	while(1)
	{
		pause();
	}
	
	return 0;
};