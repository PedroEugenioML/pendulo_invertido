#include <stdio.h>
#include <prussdrv.h>
#include <pruss_intc_mapping.h>
#include "timer.h"

int config_timer(){
    
    tpruss_intc_initdata pruss_intc_initdata = PRUSS_INTC_INITDATA;
    prussdrv_init();
    prussdrv_open(PRU_EVTOUT_1);
    prussdrv_pruintc_init(&pruss_intc_initdata);
    return 0;

};

int inicia_timer(unsigned int periodo){

    unsigned int aux = periodo/(INSTRUCOES_POR_LOOP * TEMPO_POR_INSTRUCAO);
    prussdrv_pru_write_memory(PRUSS0_PRU1_DATARAM, 0, &aux, 4);
    prussdrv_exec_program(PRU1_NUM, "./PRU/timer/timer.bin");
    return 0;
    
};

int espera_timer(){
    
    int n = prussdrv_pru_wait_event(PRU_EVTOUT_1);
    prussdrv_pru_clear_event(PRU_EVTOUT_1, PRU1_ARM_INTERRUPT);
    return n;
    
};

int encerra_timer(){
    
    prussdrv_pru_disable(PRU1_NUM);
    prussdrv_exit();
    return 0;
    
};