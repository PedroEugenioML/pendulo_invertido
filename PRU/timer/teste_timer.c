#include <stdlib.h>
#include <stdio.h>
#include "timer.h"

int main(){
    unsigned int periodo = 1000000000;
    config_timer();
    inicia_timer(periodo);
    for(int i = 0; i<10; i++){
        int n = espera_timer();
        printf("interrupção: %d\n", n);   
    }
    encerra_timer();    
};