

// Cógido do timer usando o PRU
// O algoritmo funciona assim: 
// Uma etapa inicial de configuração dos parâmetros do timer
//  - É lido na memória do PRU o número de instrução que gera o período do timer
// O programa entra no loop principla que garante inumeras interrupções
//  - Entra em um loop de delay até atingir o período do timer
//  - Sai do loop de delay e envia um sinal de interrupção (event)
//  - Retorna para o loop principal
//
// (não implementado) Caso a leitura do periodo seja nula, o programa é encerrado (Halt). 


.origin 0 
.entrypoint START 

#define PRU1_R31_VEC_VALID 32
#define PRU_EVTOUT_1 4

START:
    MOV r0, 0x00000000      // Carrega o endereço de memória onde o período foi escrito 
    LBBO r1, r0, 0, 4       // Carrega o valor do periodo armazenado na memória

MAINLOOP:
    MOV r2, 0               // Inicializa variável do loop como zero
    
DELAY:                      // Delay entre as interrupções
    ADD r2, r2, 1
    QBNE DELAY, r2, r1
    
    MOV r31.b0, PRU1_R31_VEC_VALID | PRU_EVTOUT_1 // Envia interrupção
    QBA START                                // Retorna para o loop 
    