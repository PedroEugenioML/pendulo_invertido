#ifndef TIMER_HEADER_
#define TIMER_HEADER_

#define PRU1_NUM 1
#define INSTRUCOES_POR_LOOP 2
#define TEMPO_POR_INSTRUCAO 5
int config_timer();

int inicia_timer(unsigned int periodo);

int espera_timer();

int encerra_timer();

#endif