#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdint.h>
#include <unistd.h>
#include "encoder.h"

int main(){
    
    int velocidadeA, velocidadeB;
    static unsigned int *encoder;
    if(config_encoder_pinos()!=0)
    {
        printf("Falha na configuração dos pinos do PRU\n");
    }
    inicia_PRU_encoder(&encoder);
    for(int i=0; i<30;i++)
    {
        leitura_encoder(&encoder, &velocidadeA, &velocidadeB);
        printf("Pulsos: %d, %d\n", velocidadeA, velocidadeB);
        sleep(1);
    }
    encerra_encoder();
    
    return 0;
    
};