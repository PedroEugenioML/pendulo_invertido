#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdint.h>
#include <unistd.h>
#include <prussdrv.h>
#include <pruss_intc_mapping.h>
#include "encoder.h"


int inicia_PRU_encoder(unsigned int **encoder_addr){
    unsigned int aux = 0;
    static void *pru0DataMemory;
    printf("OK\n");
    tpruss_intc_initdata pruss_intc_initdata = PRUSS_INTC_INITDATA;
    printf("OK\n");
    prussdrv_init();
    printf("OK\n");
    prussdrv_open(PRU_EVTOUT_0);
    prussdrv_pruintc_init(&pruss_intc_initdata);
    printf("OK\n");
    prussdrv_map_prumem(PRUSS0_PRU0_DATARAM, &pru0DataMemory);
    *encoder_addr = (unsigned int *)pru0DataMemory;
    printf("OK\n");
    prussdrv_pru_write_memory(PRUSS0_PRU0_DATARAM, 0, &aux, 4);
    prussdrv_pru_write_memory(PRUSS0_PRU0_DATARAM, 1, &aux, 4);
    prussdrv_exec_program(PRU0_NUM, "../PRU/encoder/encoder.bin");
    printf("OKX\n");
    return 0;
    
};

int inicia_encoder(unsigned int **encoder_addr){
    
    if(config_encoder_pinos()!=0)
    {
        printf("Falha na configuração dos pinos do PRU\n");
    }
    
    static void *pru0DataMemory;
    prussdrv_map_prumem(PRUSS0_PRU0_DATARAM, &pru0DataMemory);
    *encoder_addr = (unsigned int *)pru0DataMemory;
    printf("OK\n");
    prussdrv_exec_program(PRU0_NUM, "../PRU/encoder/encoder.bin");
    printf("OKX\n");
    return 0;
}

int leitura_encoder(unsigned int **encoder_addr, int *velocidadeA, int *velocidadeB){
    
 
    *velocidadeA = (int) **(encoder_addr);
    *velocidadeB = (int) *((*encoder_addr)+1);
    //printf("OKL\n");
    return 0;
    
};

int encerra_encoder(){
    
    prussdrv_pru_disable(PRU0_NUM);
    prussdrv_exit();
    return 0;
};

int config_encoder_pinos(){
    
    char pru_mode[5] = "pruin";
    int file_pinmux;
    
    // Configura o pino P9_28 para input do PRU
    if((file_pinmux=open("/sys/devices/platform/ocp/ocp:P9_28_pinmux/state", O_RDWR)) < 0){
		perror("Falha no acesso a P9_28_pinmux\n");
		return 1;
	}
    if( write(file_pinmux,&pru_mode,sizeof(pru_mode)) != sizeof(pru_mode) ){
        perror("Falha no acesso a 9_28_pinmux/state\n");
		return 1;
    }
    close(file_pinmux);
    
    // Configurar o pino P9_29 para input do PRU
    if((file_pinmux=open("/sys/devices/platform/ocp/ocp:P9_29_pinmux/state", O_RDWR)) < 0){
		perror("Falha no acesso a P9_29_pinmux\n");
		return 1;
	}
    if( write(file_pinmux,&pru_mode,sizeof(pru_mode)) != sizeof(pru_mode) ){
        perror("Falha no acesso a 9_29_pinmux/state\n");
		return 1;
    }
    close(file_pinmux);
    
    // Configura os pino P9_30 para input do PRU
    if((file_pinmux=open("/sys/devices/platform/ocp/ocp:P9_30_pinmux/state", O_RDWR)) < 0){
		perror("Falha no acesso a P9_30_pinmux\n");
		return 1;
	}
    if( write(file_pinmux,&pru_mode,sizeof(pru_mode)) != sizeof(pru_mode) ){
        perror("Falha no acesso a 9_30_pinmux/state\n");
		return 1;
    }
    close(file_pinmux);
    
    // Configura o pino P9_31 para input do PRU
    if((file_pinmux=open("/sys/devices/platform/ocp/ocp:P9_31_pinmux/state", O_RDWR)) < 0){
		perror("Falha no acesso a P9_31_pinmux\n");
		return 1;
	}
    if( write(file_pinmux,&pru_mode,sizeof(pru_mode)) != sizeof(pru_mode) ){
        perror("Falha no acesso a 9_31_pinmux/state\n");
		return 1;
    }
    close(file_pinmux);
    
    return 0;  
    
};