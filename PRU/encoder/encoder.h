#ifndef ENCODER_HEADER_
#define ENCODER_HEADER_

#define PRU0_NUM 0

int inicia_PRU_encoder(unsigned int **encoder_addr);

int inicia_encoder(unsigned int **encoder);

int leitura_encoder(unsigned int **encoder, int *velocidadeA, int *velocidadeB);

int encerra_encoder();

int config_encoder_pinos();

#endif