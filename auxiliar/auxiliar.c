#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <stdbool.h>
#include "auxiliar.h"

// Funcao para o sinal de interrupcao
void saida(int sig)
{
	printf("\nSinal de interrupcao capturado\nEncerrando leitura de dados\n");
        fim_loop = false;
}

// Funcao para calcular o tempo em milisegundo
int sec()
{
	struct timespec spec;
	clock_gettime(CLOCK_REALTIME, &spec);
	int time = (int)spec.tv_sec; 
	return time;
}
