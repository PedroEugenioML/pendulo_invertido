#ifndef MOTOR_HEADER_
#define MOTOR_HEADER_

// Variáveis para o controlador do motor
float kp_motor;
float ki_motor;
float kd_motor;
    
float erro_k_motor, erro_k_1_motor, erro_k_2_motor;
    
float termo_proporcional_motor;
float termo_integral_motor;
float termo_derivativo_motor;
    
float last_motor;

// Funções para o PID do motor
int inicia_PID_motor();
int controle_PID_motor(float referencial, float medido, float dt);

// Funções de inicialização
int config_pinos_PWM();
int inicia_PWM();

// Funções de configuração
int config_periodo_PWM(int periodo);
int config_duty_cycle(int velocidadeA, int velocidadeB);

int PWM_MotorA(int velocidadeA);
int PWM_MotorB(int velocidadeB);

// Funções de ativação
int liga_motores();
int desliga_motores();

int direcao_motorA(bool sentido);
int direcao_motorB(bool sentido);

// Funções de configuração dos pinos 
int direcao(int sentido);
int rotacao(int sentido);

// Funções de uso do usuário
int anda_em_X(int velocidade);
int gira_em_Z(int angulo);



#endif