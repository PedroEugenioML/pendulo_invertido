#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdbool.h>
#include <sys/ioctl.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include "motor.h"

int VEL_MAX, VEL_MIN; 

int config_pinos_PWM(){
    
    char state_pwm[3] = "pwm";
    char state_gpio[4] = "gpio";
    char gpio_direction[3] = "out";
    int file_pinmux;
    int file_direction;
    
    // configurar os pinos P9_16 para PWM
    if((file_pinmux=open("/sys/devices/platform/ocp/ocp:P9_16_pinmux/state", O_RDWR)) < 0){
		perror("Falha no acesso a P9_16_pinmux\n");
		return 1;
	}
    if( write(file_pinmux,&state_pwm,sizeof(state_pwm)) != sizeof(state_pwm) ){
        perror("Falha no acesso a P9_16_pinmux/state\n");
		return 1;
    }
    close(file_pinmux);
    
    // configurar os pinos P9_17 para GPIO
    if((file_pinmux=open("/sys/devices/platform/ocp/ocp:P9_17_pinmux/state", O_RDWR)) < 0){
		perror("Falha no acesso a P9_17_pinmux\n");
		return 1;
	}
    if( write(file_pinmux,&state_gpio,sizeof(state_gpio)) != sizeof(state_gpio) ){
        perror("Falha no acesso a P9_17_pinmux/state\n");
		return 1;
    }
    close(file_pinmux);
    // configurar a direção do P9_17 para OUT
    if((file_direction=open("/sys/class/gpio/gpio5/direction", O_RDWR)) < 0){
		perror("Falha no acesso a gpio5/direction\n");
		return 1;
	}
    if( write(file_direction,&gpio_direction,sizeof(gpio_direction)) != sizeof(gpio_direction) ){
        perror("Falha na escrita de gpio5/direction\n");
		return 1;
    }
    close(file_direction);
    
    // configurar os pinos P9_18 para GPIO
    if((file_pinmux=open("/sys/devices/platform/ocp/ocp:P9_18_pinmux/state", O_RDWR)) < 0){
		perror("Falha no acesso a P9_18_pinmux\n");
		return 1;
	}
    if( write(file_pinmux,&state_gpio,sizeof(state_gpio)) != sizeof(state_gpio) ){
        perror("Falha no acesso a P9_18_pinmux/state\n");
		return 1;
    }
    close(file_pinmux);
    // configurar a direção do P9_18 para OUT
    if((file_direction=open("/sys/class/gpio/gpio4/direction", O_RDWR)) < 0){
		perror("Falha no acesso a gpio4\n");
		return 1;
	}
    if( write(file_direction,&gpio_direction,sizeof(gpio_direction)) != sizeof(gpio_direction) ){
        perror("Falha na escrita de gpio4/direction\n");
		return 1;
    }
    close(file_direction);
    
    // configurar os pinos P9_21 para PWM
    if((file_pinmux=open("/sys/devices/platform/ocp/ocp:P9_22_pinmux/state", O_RDWR)) < 0){
		perror("Falha no acesso a P9_22_pinmux\n");
		return 1;
	}
    if( write(file_pinmux,&state_pwm,sizeof(state_pwm)) != sizeof(state_pwm) ){
        perror("Falha no acesso a P9_22_pinmux/state\n");
		return 1;
    }
    close(file_pinmux);
    
    // configurar os pinos P9_24 para GPIO
    if((file_pinmux=open("/sys/devices/platform/ocp/ocp:P9_24_pinmux/state", O_RDWR)) < 0){
		perror("Falha no acesso a P9_24_pinmux\n");
		return 1;
	}
    if( write(file_pinmux,&state_gpio,sizeof(state_gpio)) != sizeof(state_gpio) ){
        perror("Falha no acesso a P9_24_pinmux/state\n");
		return 1;
    }
    close(file_pinmux);
    // configurar a direção do P9_24 para OUT
    if((file_direction=open("/sys/class/gpio/gpio15/direction", O_RDWR)) < 0){
		perror("Falha no acesso a gpio15\n");
		return 1;
	}
    if( write(file_direction,&gpio_direction,sizeof(gpio_direction)) != sizeof(gpio_direction) ){
        perror("Falha na escrita de gpio15/direction\n");
		return 1;
    }
    close(file_direction);
    
    // configurar os pinos P9_26 para GPIO
    if((file_pinmux=open("/sys/devices/platform/ocp/ocp:P9_26_pinmux/state", O_RDWR)) < 0){
		perror("Falha no acesso a P9_26_pinmux\n");
		return 1;
	}
    if( write(file_pinmux,&state_gpio,sizeof(state_gpio)) != sizeof(state_gpio) ){
        perror("Falha no acesso a P9_26_pinmux/state\n");
		return 1;
    }
    close(file_pinmux);
    // configurar a direção do P9_26 para OUT
    if((file_direction=open("/sys/class/gpio/gpio14/direction", O_RDWR)) < 0){
		perror("Falha no acesso a gpio14\n");
		return 1;
	}
    if( write(file_direction,&gpio_direction,sizeof(gpio_direction)) != sizeof(gpio_direction) ){
        perror("Falha na escrita de gpio14/direction\n");
		return 1;
    }
    close(file_direction);
    
    // Pino STBY
    // configurar os pinos P9_23 para GPIO 
    if((file_pinmux=open("/sys/devices/platform/ocp/ocp:P9_23_pinmux/state", O_RDWR)) < 0){
		perror("Falha no acesso a P9_23_pinmux\n");
		return 1;
	}
    if( write(file_pinmux,&state_gpio,sizeof(state_gpio)) != sizeof(state_gpio) ){
        perror("Falha no acesso a P9_23_pinmux/state\n");
		return 1;
    }
    close(file_pinmux);
    // configurar a direção do P9_26 para OUT
    if((file_direction=open("/sys/class/gpio/gpio49/direction", O_RDWR)) < 0){
		perror("Falha no acesso a gpio49\n");
		return 1;
	}
    if( write(file_direction,&gpio_direction,sizeof(gpio_direction)) != sizeof(gpio_direction) ){
        perror("Falha na escrita de gpio49/direction\n");
		return 1;
    }
    close(file_direction);
    
    return 0;
};

int inicia_PWM(){
    
    int file_export;
    char pwm0[1] = "0";
    char pwm2[1] = "1";
    
    
    // Exporta o PWM0A
    if((file_export=open("/sys/class/pwm/pwmchip1/export", O_WRONLY)) < 0){
		perror("Falha no acesso a /pwmchip1/export\n");
		//return 1;
	}
    if( write(file_export,&pwm0,sizeof(pwm0)) != sizeof(pwm0) ){
        perror("Falha na escrita de /pwmchip1/export\n");
    	//return 1;
    }
    else{
        printf("export1 ok\n");
    }
    close(file_export);
   
   // Exporta o PWM1B
   if((file_export=open("/sys/class/pwm/pwmchip4/export", O_WRONLY)) < 0){
		perror("Falha no acesso a /pwmchip4/export\n");
		//return 1;
	}
	else{
        if( write(file_export,&pwm2,sizeof(pwm2)) != sizeof(pwm2) ){
            perror("Falha na escrita de /pwmchip4/export\n");
    	    //return 1;
        }
        else{
            printf("export4 ok\n");
        }
    }
    close(file_export);
    
    config_periodo_PWM(100000);
    return 0;
};

int config_periodo_PWM(int periodo){
    
    int file_period;
    char period[15] = {0};
    sprintf(period, "%d", periodo);
    // Ajusta os valores máximos para o duty cycle com base no período
    VEL_MAX = periodo;
    VEL_MIN = (periodo/4);
    
    // Configura o periodo do pwm0
   if((file_period=open("/sys/class/pwm/pwmchip1/pwm-1:0/period", O_RDWR)) < 0){
		perror("Falha no acesso a /pwmchip1/pwm-1:0/period\n");
		//return 1;
	}
	else{
        if( write(file_period,&period,sizeof(period)) != sizeof(period) ){
            perror("Falha na escrita de /pwmchip1/pwm-1:0/period\n");
    		//return 1;
        }
        printf("escrita chip1 ok\n");
    }
    close(file_period);
    
    // Configura o periodo do pwm2
   if((file_period=open("/sys/class/pwm/pwmchip4/pwm-4:1/period", O_RDWR)) < 0){
		perror("Falha no acesso a /pwmchip4/pwm-4:1/period\n");
		return 1;
	}
    if( write(file_period,&period,sizeof(period)) != sizeof(period) ){
        perror("Falha na escrita de /pwmchip4/pwm-4:1/period\n");
		return 1;
    }
    printf("ok final\n");
    close(file_period);
    return 0;
};

int desliga_motores(){
    
    int file_enable;
    char disable[1] = "0";
    
    if((file_enable=open("/sys/class/pwm/pwmchip1/pwm-1:0/enable", O_RDWR)) < 0){
		perror("Falha no acesso a /pwmchip1/pwm-1:0/enable\n");
		//return 1;
	}
	else{
        if( write(file_enable,&disable,sizeof(disable)) != sizeof(disable) ){
            perror("Falha na escrita de /pwmchip1/pwm-1:0/enable\n");
    		//return 1;
        }
        printf("disable chip1\n");
    }
    close(file_enable);
    
    if((file_enable=open("/sys/class/pwm/pwmchip4/pwm-4:1/enable", O_RDWR)) < 0){
		perror("Falha no acesso a /pwmchip4/pwm-4:1/enable\n");
		//return 1;
	}
	else{
        if( write(file_enable,&disable,sizeof(disable)) != sizeof(disable) ){
            perror("Falha na escrita de /pwmchip4/pwm-4:1/enable\n");
    		//return 1;
        }
        printf("disable chip4 \n");
    }
    close(file_enable);
    
    if((file_enable=open("/sys/class/gpio/gpio49/value", O_WRONLY)) < 0){
    	perror("Falha no acesso a /gpio49/value\n");
    }
    else{
        if( write(file_enable,&disable,sizeof(disable)) != sizeof(disable) ){
            perror("Falha na escrita a /gpio49/value\n");
        }
    }
    close(file_enable);
    return 0;
};

int liga_motores(){
    
    int file_enable;
    char enable[1] = "1";
    
    // Habilita o PWM0A
    if((file_enable=open("/sys/class/pwm/pwmchip1/pwm-1:0/enable", O_RDWR)) < 0){
		perror("Falha no acesso a /pwmchip1/pwm-1:0/enable\n");
		//return 1;
	}
	else{
        if( write(file_enable,&enable,sizeof(enable)) != sizeof(enable) ){
            perror("Falha na escrita de /pwmchip1/pwm-1:0/enable\n");
    		//return 1;
        }
        printf("enable chip1\n");
    }
    close(file_enable);
    
    // Habilita o PWM1B
    if((file_enable=open("/sys/class/pwm/pwmchip4/pwm-4:1/enable", O_RDWR)) < 0){
		perror("Falha no acesso a /pwmchip4/pwm-4:1/enable\n");
		//return 1;
	}
	else{
        if( write(file_enable,&enable,sizeof(enable)) != sizeof(enable) ){
            perror("Falha na escrita de /pwmchip4/pwm-4:1/enable\n");
    		//return 1;
        }
        printf("enable chip4\n");
    }
    close(file_enable);
    
    if((file_enable=open("/sys/class/gpio/gpio49/value", O_WRONLY)) < 0){
    	perror("Falha no acesso a /gpio49/value\n");
    }
    else{
        if( write(file_enable,&enable,sizeof(enable)) != sizeof(enable) ){
            perror("Falha na escrita a /gpio49/value\n");
        }
    }
    close(file_enable);
    
    return 0;
};

int PWM_MotorA(int velocidadeA){
    
    // velocidadeA é o duty cucle de 0% a 100%
    // Ajuste para o valor do width pulse do PWM
    velocidadeA = (int ) (velocidadeA * VEL_MAX)/100;
    
    int file_duty_cycle;
    char duty_cycleA[20] = {0};   

    if(velocidadeA>VEL_MAX){
        velocidadeA = VEL_MAX;
    }
    if(velocidadeA<VEL_MIN){
        velocidadeA = VEL_MIN;
    }
    sprintf(duty_cycleA, "%d", velocidadeA);
    
    if((file_duty_cycle=open("/sys/class/pwm/pwmchip1/pwm-1:0/duty_cycle", O_RDWR)) < 0){
		perror("Falha no acesso a /pwmchip1/pwm-1:0/duty_cycle\n");
		//return 1;
	}
	else{
        if( write(file_duty_cycle,&duty_cycleA,sizeof(duty_cycleA)) != sizeof(duty_cycleA) ){
            perror("Falha na escrita de /pwmchip1/pwm-1:0/duty_cycle\n");
    		//return 1;
        }
        //printf("duty cycle A ok\n");
    }
    close(file_duty_cycle);
    return 0;
};

    
int PWM_MotorB(int velocidadeB){
    
    // velocidadeA é o duty cucle de 0% a 100%
    // Ajuste para o valor do width pulse do PWM
    velocidadeB = (velocidadeB * VEL_MAX)/100;
    
    int file_duty_cycle;
    char duty_cycleB[20] = {0};   


    if(velocidadeB>VEL_MAX){
        velocidadeB = VEL_MAX;
    }
    if(velocidadeB<VEL_MIN){
        velocidadeB = VEL_MIN;
    }
    sprintf(duty_cycleB, "%d", velocidadeB);
    
    if((file_duty_cycle=open("/sys/class/pwm/pwmchip4/pwm-4:1/duty_cycle", O_RDWR)) < 0){
		perror("Falha no acesso a /pwmchip4/pwm-4:1/duty_cycle\n");
		//return 1;
	}
	else{
        if( write(file_duty_cycle,&duty_cycleB,sizeof(duty_cycleB)) != sizeof(duty_cycleB) ){
            perror("Falha na escrita de /pwmchip4/pwm-4:1/duty_cycle\n");
    		//return 1;
        }
        //printf("duty cycle B ok\n");
    }
    close(file_duty_cycle);
    return 0;
};

int config_duty_cycle(int velocidadeA, int velocidadeB){
    
    int file_duty_cycle;
    char duty_cycleA[20] = {0};
    char duty_cycleB[20] = {0};    

    if(velocidadeA>VEL_MAX){
        velocidadeA = VEL_MAX;
    }
    if(velocidadeA<VEL_MIN){
        velocidadeA = VEL_MIN;
    }
    sprintf(duty_cycleA, "%d", velocidadeA);
    
    if((file_duty_cycle=open("/sys/class/pwm/pwmchip1/pwm-1:0/duty_cycle", O_RDWR)) < 0){
		perror("Falha no acesso a /pwmchip1/pwm-1:0/duty_cycle\n");
		//return 1;
	}
	else{
        if( write(file_duty_cycle,&duty_cycleA,sizeof(duty_cycleA)) != sizeof(duty_cycleA) ){
            perror("Falha na escrita de /pwmchip1/pwm-1:0/duty_cycle\n");
    		//return 1;
        }
        //printf("duty cycle A ok\n");
    }
    close(file_duty_cycle);
    
    if(velocidadeB>VEL_MAX){
        velocidadeB = VEL_MAX;
    }
    if(velocidadeB<VEL_MIN){
        velocidadeB = VEL_MIN;
    }
    sprintf(duty_cycleB, "%d", velocidadeB);
    
    if((file_duty_cycle=open("/sys/class/pwm/pwmchip4/pwm-4:1/duty_cycle", O_RDWR)) < 0){
		perror("Falha no acesso a /pwmchip4/pwm-4:1/duty_cycle\n");
		//return 1;
	}
	else{
        if( write(file_duty_cycle,&duty_cycleB,sizeof(duty_cycleB)) != sizeof(duty_cycleB) ){
            perror("Falha na escrita de /pwmchip4/pwm-4:1/duty_cycle\n");
    		//return 1;
        }
        //printf("duty cycle B ok\n");
    }
    close(file_duty_cycle);
    return 0;
};

int anda_em_X(int velocidade){
    
    // Desabilita o motor para evitar desarranjos
    desliga_motores();
    config_duty_cycle(abs(velocidade),abs(velocidade));
    direcao(velocidade);
    liga_motores();
    return 0;
};

int gira_em_Z(int angulo){
    
    // Desabilita o motor para evitar desarranjos
    desliga_motores();
    config_duty_cycle(abs(angulo),abs(angulo));
    rotacao(angulo);
    liga_motores();
    return 0;
};


int direcao_motorA(bool sentido){
    
    char set[1] = "1";
    char reset[1] = "0";
    int file_value;
    
    if(sentido==true){

        if((file_value=open("/sys/class/gpio/gpio15/value", O_WRONLY)) < 0){
    		perror("Falha no acesso a /gpio5/value\n");
    	}
    	else{
            if( write(file_value,&set,sizeof(set)) != sizeof(set) ){
                perror("Falha na escrita a /gpio15/value\n");
            }
    	}
    	close(file_value);
    	
    	if((file_value=open("/sys/class/gpio/gpio14/value", O_WRONLY)) < 0){
    		perror("Falha no acesso a /gpio14/value\n");
    	}
    	else{
            if( write(file_value,&reset,sizeof(reset)) != sizeof(reset) ){
                perror("Falha na escrita a /gpio14/value\n");
            }
    	}
    	close(file_value);
    }
    else
    {
        if((file_value=open("/sys/class/gpio/gpio15/value", O_WRONLY)) < 0){
    		perror("Falha no acesso a /gpio5/value\n");
    	}
    	else{
            if( write(file_value,&reset,sizeof(reset)) != sizeof(reset) ){
                perror("Falha na escrita a /gpio15/value\n");
            }
    	}
    	close(file_value);
    	
    	if((file_value=open("/sys/class/gpio/gpio14/value", O_WRONLY)) < 0){
    		perror("Falha no acesso a /gpio14/value\n");
    	}
    	else{
            if( write(file_value,&set,sizeof(set)) != sizeof(set) ){
                perror("Falha na escrita a /gpio14/value\n");
            }
    	}
    	close(file_value);
        
    }
    return 0;
};


int direcao_motorB(bool sentido){
    
    char set[1] = "1";
    char reset[1] = "0";
    int file_value;
    
    if(sentido==true){

        if((file_value=open("/sys/class/gpio/gpio5/value", O_WRONLY)) < 0){
    		perror("Falha no acesso a /gpio5/value\n");
    	}
    	else{
            if( write(file_value,&set,sizeof(set)) != sizeof(set) ){
                perror("Falha na escrita a /gpio5/value\n");
            }
    	}
    	close(file_value);
    	
    	if((file_value=open("/sys/class/gpio/gpio4/value", O_WRONLY)) < 0){
    		perror("Falha no acesso a /gpio4/value\n");
    	}
    	else{
            if( write(file_value,&reset,sizeof(reset)) != sizeof(reset) ){
                perror("Falha na escrita a /gpio4/value\n");
            }
    	}
    	close(file_value);
    }
    else
    {
        if((file_value=open("/sys/class/gpio/gpio5/value", O_WRONLY)) < 0){
    		perror("Falha no acesso a /gpio5/value\n");
    	}
    	else{
            if( write(file_value,&reset,sizeof(reset)) != sizeof(reset) ){
                perror("Falha na escrita a /gpio5/value\n");
            }
    	}
    	close(file_value);
    	
    	if((file_value=open("/sys/class/gpio/gpio4/value", O_WRONLY)) < 0){
    		perror("Falha no acesso a /gpio4/value\n");
    	}
    	else{
            if( write(file_value,&set,sizeof(set)) != sizeof(set) ){
                perror("Falha na escrita a /gpio4/value\n");
            }
    	}
    	close(file_value);
        
    }
    return 0;
};

int direcao(int sentido){
    
    char set[1] = "1";
    char reset[1] = "0";
    int file_value;
    
    if(sentido>=0){
        /*
            motor1 CW
            IN1 = 1, IN2 = 0
            motor2 CCW
            IN1 = 0, IN2 = 1
        */ 
        if((file_value=open("/sys/class/gpio/gpio15/value", O_WRONLY)) < 0){
    		perror("Falha no acesso a /gpio5/value\n");
    	}
    	else{
            if( write(file_value,&set,sizeof(set)) != sizeof(set) ){
                perror("Falha na escrita a /gpio15/value\n");
            }
    	}
    	close(file_value);
    	
    	if((file_value=open("/sys/class/gpio/gpio14/value", O_WRONLY)) < 0){
    		perror("Falha no acesso a /gpio14/value\n");
    	}
    	else{
            if( write(file_value,&reset,sizeof(reset)) != sizeof(reset) ){
                perror("Falha na escrita a /gpio14/value\n");
            }
    	}
    	close(file_value);
    	
    	if((file_value=open("/sys/class/gpio/gpio5/value", O_WRONLY)) < 0){
    		perror("Falha no acesso a /gpio5/value\n");
    	}
    	else{
            if( write(file_value,&reset,sizeof(reset)) != sizeof(reset) ){
                perror("Falha na escrita a /gpio5/value\n");
            }
    	}
    	close(file_value);
    	
    	if((file_value=open("/sys/class/gpio/gpio4/value", O_WRONLY)) < 0){
    		perror("Falha no acesso a /gpio4/value\n");
    	}
    	else{
            if( write(file_value,&set,sizeof(set)) != sizeof(set) ){
                perror("Falha na escrita a /gpio4/value\n");
            }
    	}
    	close(file_value);
    }
    
    if(sentido<0){
        /*
            motor1 CW
            IN1 = 0, IN2 = 1
            motor2 CCW
            IN1 = 1, IN2 = 0
        */ 
        if((file_value=open("/sys/class/gpio/gpio15/value", O_WRONLY)) < 0){
    		perror("Falha no acesso a /gpio15/value\n");
    	}
    	else{
            if( write(file_value,&reset,sizeof(reset)) != sizeof(reset) ){
                perror("Falha na escrita a /gpio15/value\n");
            }
    	}
    	close(file_value);
    	
    	if((file_value=open("/sys/class/gpio/gpio14/value", O_WRONLY)) < 0){
    		perror("Falha no acesso a /gpio14/value\n");
    	}
    	else{
            if( write(file_value,&set,sizeof(set)) != sizeof(set) ){
                perror("Falha na escrita a /gpio14/value\n");
            }
    	}
    	close(file_value);
    	
    	if((file_value=open("/sys/class/gpio/gpio5/value", O_WRONLY)) < 0){
    		perror("Falha no acesso a /gpio5/value\n");
    	}
    	else{
            if( write(file_value,&set,sizeof(set)) != sizeof(set) ){
                perror("Falha na escrita a /gpio5/value\n");
            }
    	}
    	close(file_value);
    	
    	if((file_value=open("/sys/class/gpio/gpio4/value", O_WRONLY)) < 0){
    		perror("Falha no acesso a /gpio4/value\n");
    	}
    	else{
            if( write(file_value,&reset,sizeof(reset)) != sizeof(reset) ){
                perror("Falha na escrita a /gpio4/value\n");
            }
    	}
    	close(file_value);
    } 
    return 0;
};

int rotacao(int sentido){
    
    char set[1] = "1";
    char reset[1] = "0";
    int file_value;
    
    if(sentido>=0){
        /*
            motor1 CW
            IN1 = 1, IN2 = 0
            motor2 CCW
            IN1 = 1, IN2 = 0
        */ 
        if((file_value=open("/sys/class/gpio/gpio15/value", O_WRONLY)) < 0){
    		perror("Falha no acesso a /gpio15/value\n");
    	}
    	else{
            if( write(file_value,&set,sizeof(set)) != sizeof(set) ){
                perror("Falha na escrita a /gpio15/value\n");
            }
    	}
    	close(file_value);
    	
    	if((file_value=open("/sys/class/gpio/gpio14/value", O_WRONLY)) < 0){
    		perror("Falha no acesso a /gpio14/value\n");
    	}
    	else{
            if( write(file_value,&reset,sizeof(reset)) != sizeof(reset) ){
                perror("Falha na escrita a /gpio14/value\n");
            }
    	}
    	close(file_value);
    	
    	if((file_value=open("/sys/class/gpio/gpio5/value", O_WRONLY)) < 0){
    		perror("Falha no acesso a /gpio5/value\n");
    	}
    	else{
            if( write(file_value,&set,sizeof(set)) != sizeof(set) ){
                perror("Falha na escrita a /gpio5/value\n");
            }
    	}
    	close(file_value);
    	
    	if((file_value=open("/sys/class/gpio/gpio4/value", O_WRONLY)) < 0){
    		perror("Falha no acesso a /gpio4/value\n");
    	}
    	else{
            if( write(file_value,&reset,sizeof(reset)) != sizeof(reset) ){
                perror("Falha na escrita a /gpio4/value\n");
            }
    	}
    	close(file_value);
    }
    
    if(sentido<0){
        /*
            motor1 CW
            IN1 = 0, IN2 = 1
            motor2 CCW
            IN1 = 0, IN2 = 1
        */ 
        if((file_value=open("/sys/class/gpio/gpio15/value", O_WRONLY)) < 0){
    		perror("Falha no acesso a /gpio15/value\n");
    	}
    	else{
            if( write(file_value,&reset,sizeof(reset)) != sizeof(reset) ){
                perror("Falha na escrita a /gpio15/value\n");
            }
    	}
    	close(file_value);
    	
    	if((file_value=open("/sys/class/gpio/gpio14/value", O_WRONLY)) < 0){
    		perror("Falha no acesso a /gpio14/value\n");
    	}
    	else{
            if( write(file_value,&set,sizeof(set)) != sizeof(set) ){
                perror("Falha na escrita a /gpio14/value\n");
            }
    	}
    	close(file_value);
    	
    	if((file_value=open("/sys/class/gpio/gpio5/value", O_WRONLY)) < 0){
    		perror("Falha no acesso a /gpio5/value\n");
    	}
    	else{
            if( write(file_value,&reset,sizeof(reset)) != sizeof(reset) ){
                perror("Falha na escrita a /gpio5/value\n");
            }
    	}
    	close(file_value);
    	
    	if((file_value=open("/sys/class/gpio/gpio4/value", O_WRONLY)) < 0){
    		perror("Falha no acesso a /gpio4/value\n");
    	}
    	else{
            if( write(file_value,&set,sizeof(set)) != sizeof(set) ){
                perror("Falha na escrita a /gpio4/value\n");
            }
    	}
    	close(file_value);
    }  
    return 0;
};

int inicia_PID_motor(){
    kp_motor = 0.1;
    ki_motor = 0.09;
    kd_motor = 0.0;
    
    return 0;
};

int controle_PID_motor(float referencial, float medido, float dt){
	
	printf("\nKp: %f, Ki: %f, Kd: %f\n", kp_motor, ki_motor, kd_motor);	
	erro_k_motor = referencial - medido;
	printf("erro: %f, %f, ref: %f, med: %f\n", erro_k_motor, erro_k_1_motor, referencial, medido);	
	// Cálculo da parcela proporcional
	termo_proporcional_motor = kp_motor * (erro_k_motor - erro_k_1_motor);
	// Cálculo da parcela integral
	termo_integral_motor = ( (ki_motor*dt)/2 ) * (erro_k_motor + erro_k_1_motor);
	// Cálculo da parcela derivativa
    //	termo_derivativo_motor = ( kd_motor/dt ) * (erro_k_motor - (2* erro_k_1_motor) + erro_k_2_motor);
	
	printf("P: %f, I: %f, D: %f\n", termo_proporcional_motor, termo_integral_motor, termo_derivativo_motor);
	// Limite para saturação da parcela integral
	if(termo_integral_motor>75){
	    termo_integral_motor = 75;
	}
	if(termo_integral_motor<-75){
	    termo_integral_motor = -75;
	}
	
	if(termo_derivativo_motor>50){
	    termo_derivativo_motor = 50;
	}
	if(termo_derivativo_motor<-50){
	    termo_derivativo_motor = 50;
	}
	
	// Cálculo do sinal
    int sinal = (int ) ( last_motor + termo_proporcional_motor + termo_integral_motor + termo_derivativo_motor );
    
	// Limitação do sinal de saída do PID
	if(sinal>100){
	    sinal = 100;
	}
	if(sinal < -100){
	    sinal = -100;
	}
	
	// Atualização das variáveis
	last_motor = sinal;
	erro_k_1_motor = erro_k_motor;
	erro_k_2_motor = erro_k_1_motor;
	
	// retorna sinal de saída do PID
	return sinal;
}