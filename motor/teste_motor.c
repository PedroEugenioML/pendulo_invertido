#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <stdbool.h>
#include "motor.h"
#include "../PRU/encoder/encoder.h"
#include "../auxiliar/auxiliar.h"

int main(){
    
    signal(SIGINT, saida);
    fim_loop=true;
    
    int pos_motorA = 0, pos_motorB = 0;
    bool direcao_A = false, direcao_B = false;
    static unsigned int *encoder;
    if(config_encoder_pinos()!=0)
    {
        printf("Falha na configuração dos pinos do PRU\n");
    }
    inicia_PRU_encoder(&encoder);
    
    config_pinos_PWM();
    inicia_PWM();
    //config_periodo_PWM(1000000); 
    
    liga_motores();
    //direcao_motorA(direcao_A);
    //direcao_motorB(direcao_B);
    //PWM_MotorA(50);
    //PWM_MotorB(30);
    
    int distance = 1135;
    int dt = 0.01;
    int sinal_motor_A = 0, sinal_motor_B = 0;
    inicia_PID_motor();
    
    while(fim_loop)
    {

        leitura_encoder(&encoder, &pos_motorB, &pos_motorA);
        sinal_motor_A = controle_PID_motor(distance, pos_motorA, dt);
        //sinal_motor_B = controle_PID_motor(distance, pos_motorB, dt);
        
        if(sinal_motor_A>=0){
            direcao_A = false;
        }
        else{
            direcao_A = true;
        }
        
        if(sinal_motor_B>=0){
            direcao_B = false;
        }
        else{
            direcao_B = true;
        }
        direcao_motorA(direcao_A);
        PWM_MotorA(abs(sinal_motor_A));
        //PWM_MotorB(abs(sinal_motor_B));
        
        printf("encoder A: %d DIR: %d, sinal: %d, encoder B: %d DIR: %d, sinal: %d\n", pos_motorA, direcao_A, sinal_motor_A, pos_motorB, direcao_B, sinal_motor_B);
        usleep(10000);
    }
    desliga_motores();
    printf("THE END\n");
    return 0;  
};







/*
    int i=0;
    
    while(fim_loop)
    {
        while(i<=100)
        {
            PWM_MotorA(i);
            PWM_MotorB(i);
            usleep(100000);
            i++;
        }
        //sleep(1);
        while(i>=0)
        {
            PWM_MotorA(i);
            PWM_MotorB(i);
            usleep(100000);
            i--;
        }
        sleep(1);
        
        leitura_encoder(&encoder, &pos_motorB, &pos_motorA);
        printf("encoder A: %d DIR: %d, encoder B: %d DIR: %d\n", pos_motorA, direcao_A, pos_motorB, direcao_B);
        direcao_A = !direcao_A;
        direcao_B = !direcao_B;
        direcao_motorA(direcao_A);
        direcao_motorB(direcao_B);
    }
*/