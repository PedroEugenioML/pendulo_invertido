#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <pthread.h>
#include <stdint.h>
#include <unistd.h>
#include <signal.h>
#include <sys/signal.h>
#include <time.h>
#include <sys/time.h>
#include <stdbool.h>
#include "../sensor/MPU9250.h"
#include "../sensor/gyroscope.h"
#include "../sensor/accelerometer.h"
#include "../sensor/magnetometer.h"
#include "../atitude/atitude.h"
#include "filtro.h"
#include "kalman.h"

int n_amostra;

int file; //
int dt = 0.01;
double offset_accel[3];
double offset_gyro[3];
double gyro[3];
double angulos_filtrados[3];
double angulos_sensor[3];

FILE * dados_accel;
FILE * dados_filtro;

Kalman kalmanX;
Kalman kalmanY;

void alarme_funcao(int signum){
    
    atitude_accel(file, offset_accel, angulos_sensor);
    leitura_gyro(file, offset_gyro, gyro);
    
    angulos_filtrados[0] = get_Kalman(&kalmanX, angulos_sensor[0], gyro[0], dt);
    
    fprintf(dados_accel,"%f ", angulos_sensor[0]);
	fprintf(dados_filtro,"%f ", angulos_filtrados[0]);
	
	printf("Filtro: %f, sensor: %f\n", angulos_filtrados[0], angulos_sensor[0]);
	
	if(n_amostra>=1000){
	    ualarm(0, 0);
	    
	    printf("FIM\n");
	    
	    close(file);
	    fclose(dados_accel);
	    fclose(dados_filtro);
	    
	    signal(SIGINT, SIG_DFL);
	    n_amostra++;
	}
    else{
        ualarm(10000, 0);
    }    
};

void funcao_saida(int signum)
{
    ualarm(0, 0);
    
    printf("FIM\n");
    
	close(file);
	fclose(dados_accel);
	fclose(dados_filtro);
	
	signal(SIGINT, SIG_DFL);
};

int main(){
    
    signal(SIGINT, funcao_saida);
    signal(SIGALRM, alarme_funcao);

	n_amostra = 0; 
	if( (file=inicia_i2c()) <= 0){
		printf("Falha na abertura do I2C\n");
		return 1;
	}
	printf("I2C iniciada\n");
	
	if(inicia_MPU9250(file)<0)
	{
		printf("Falha na inicialização da MPU9250\n");
		return 1;
	}
	printf("IMU iniciada\n");
	
	if(config_MPU9250(file, GYRO_FULL_SCALE_250_DPS, ACCEL_FULL_SCALE_2_G) < 0)
	{
		printf("Falha na configuração da MPU9250\n");
		return 1;
	}
	
	if(calibra_gyro(file, offset_gyro)<0)
	{
		printf("Erro na calibração\n");
		return 1;
	}
	
    if(calibra_accel(file, offset_accel)<0)
	{
		printf("Erro na calibração\n");
		return 1;
	}
	
	dados_accel=fopen("accel.txt", "w");
	if(dados_accel == NULL)
	{
		printf("Falha na abertura do arquivo accel\n");
		return 1;
	}
	dados_filtro=fopen("filtro.txt", "w");
	if(dados_filtro == NULL)
	{
		printf("Falha na abertura do arquivo filtro\n");
		return 1;
	}
	printf("Arquivo aberto\n");
	
	printf("Iniciando leitura\n");
	sleep(3);
	
	inicia_Kalman(&kalmanX);
	inicia_Kalman(&kalmanY);
	
	atitude_accel(file, offset_accel, angulos_sensor);
	
	set_Kalman(&kalmanX, angulos_sensor[0]);
	set_Kalman_bias(&kalmanX, offset_accel[0]);
	
	ualarm(10000, 0);
	
	while(1){
	    pause();
	}
	
}