#ifndef FILTRO_HEADER_
#define FILTRO_HEADER_

#define HPF 0.95
#define LPF 0.05


int filtro_complementar(int file, double *offset_accel, double *offset_gyro, double *angulos_fc, double dt);


#endif