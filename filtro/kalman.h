#ifndef KALMAN_HEADER_
#define KALMAN_HEADER_

typedef struct
{
    /* We will set the variables like so, these can also be tuned by the user */
    double Q_angle;
    double Q_bias;
    double R_measure;

    double angle; // Reset the angle
    double bias; // Reset bias
    double rate;
     
    double P[2][2];
    
}Kalman;

void inicia_Kalman(Kalman *variavel);
void set_Kalman(Kalman *variavel, double angulo);
void set_Kalman_bias(Kalman *variavel, double bias);
double get_Kalman(Kalman *variavel, double newAngle, double newRate, double dt);

#endif