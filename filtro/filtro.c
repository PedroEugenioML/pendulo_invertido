#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include "filtro.h"
#include "../sensor/gyroscope.h"
#include "../atitude/atitude.h"


int filtro_complementar(int file, double *offset_accel, double *offset_gyro, double *angulos_fc, double dt){
  	
  	double accel_fc[3];
  	double gyro_fc[3];
  	
  	if( atitude_accel(file, offset_accel, accel_fc) < 0 )
  	{
  	    printf("Falha na orientação do acelerômetro\n");
  	    return -1;
  	}
		
	if(leitura_gyro(file, offset_gyro, gyro_fc)<0)
	{
	    printf("Falha na leitura do gyroscópio\n");
	    return -1;
	}	
	
	angulos_fc[0] = HPF*(angulos_fc[0] + gyro_fc[0]*(dt)) + LPF*accel_fc[0];
	angulos_fc[1] = HPF*(angulos_fc[1] + gyro_fc[1]*(dt)) + LPF*accel_fc[1];
	angulos_fc[2] = HPF*(angulos_fc[2] + gyro_fc[2]*(dt)) + LPF*accel_fc[2];  
	
	return 0;
};
