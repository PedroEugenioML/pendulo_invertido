#include <stdio.h>
#include <stdlib.h>
#include "kalman.h"


void inicia_Kalman(Kalman *variavel){
    /* We will set the variables like so, these can also be tuned by the user */
    variavel->Q_angle = 10.1;
    variavel->Q_bias = 10.3;
    variavel->R_measure = 10.3;

    variavel->angle = 0.0; // Reset the angle
    variavel->bias = 0.0; // Reset bias

    variavel->P[0][0] = 0.01; // Since we assume that the bias is 0 and we know the starting angle (use setAngle), the error covariance matrix is set like so - see: http://en.wikipedia.org/wiki/Kalman_filter#Example_application.2C_technical
    variavel->P[0][1] = 0.01;
    variavel->P[1][0] = 0.01;
    variavel->P[1][1] = 0.01;
};

void set_Kalman(Kalman *variavel, double angulo){
    variavel->angle = angulo; 
};

void set_Kalman_bias(Kalman *variavel, double bias){
    variavel->bias = bias; 
};

double get_Kalman(Kalman *variavel, double newAngle, double newRate, double dt){
    
    printf("Atualizando Kalman\n");
    
    // Discrete Kalman filter time update equations - Time Update ("Predict")
    // Update xhat - Project the state ahead
    /* Step 1 */
    variavel->rate = newRate - variavel->bias;
    variavel->angle += variavel->rate * dt;
    printf("rate: %f, newRate: %f, angle: %f, bias: %f\n", variavel->rate, newRate, variavel->angle, variavel->bias);
    // Update estimation error covariance - Project the error covariance ahead
    /* Step 2 */
    variavel->P[0][0] += ( (variavel->P[1][1] * dt) - variavel->P[0][1] - variavel->P[1][0] + variavel->Q_angle) * dt;
    variavel->P[0][1] -= variavel->P[1][1] * dt;
    variavel->P[1][0] -= variavel->P[1][1] * dt;
    variavel->P[1][1] += variavel->Q_bias * dt;

    printf("P: %f, %f, %f, %f\n", variavel->P[0][0], variavel->P[0][1], variavel->P[1][0], variavel->P[1][1]);
    // Discrete Kalman filter measurement update equations - Measurement Update ("Correct")
    // Calculate Kalman gain - Compute the Kalman gain
    /* Step 4 */
    double S = variavel->P[0][0] + variavel->R_measure; // Estimate error
    /* Step 5 */
    double K[2]; // Kalman gain - This is a 2x1 vector
    K[0] = variavel->P[0][0] / S;
    K[1] = variavel->P[1][0] / S;

    // Calculate angle and bias - Update estimate with measurement zk (newAngle)
    /* Step 3 */
    double y = newAngle - variavel->angle; // Angle difference
    /* Step 6 */
    variavel->angle += K[0] * y;
    variavel->bias += K[1] * y;

    // Calculate estimation error covariance - Update the error covariance
    /* Step 7 */
    double P00_temp = variavel->P[0][0];
    double P01_temp = variavel->P[0][1];

    variavel->P[0][0] -= K[0] * P00_temp;
    variavel->P[0][1] -= K[0] * P01_temp;
    variavel->P[1][0] -= K[1] * P00_temp;
    variavel->P[1][1] -= K[1] * P01_temp;

    return variavel->angle;
};